// const _ = require('lodash')
// // 要柯里化的函数
// function getSum(a, b, c) {
// 	return a + b + c
// }
// // 柯里化后的函数
// let curried = _.curry(getSum)
// // 测试
// console.log(curried(1, 2, 3)) // 6
// console.log(curried(1)(2)(3)) // 6
// console.log(curried(1, 2)(3)) // 6

// 一个容器，包裹一个值
class Container {
  // of 静态方法，可以省略new关键字创建对象
	static of(value) {
  	return new Container(value)
  }
  constructor(value) {
  	this._value = value
  }
  // map方法，传入变形关系，将容器里的每一个值映射到另一个容器
  map(fn) {
  	return Container.of(fn(this._value))
  }
}

// 测试
console.log(Container.of(3).map(x => x + 2).map(x => x * x))