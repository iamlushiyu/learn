class MyPromise {
    constructor(executor) {
        executor(this.resolve, this.reject)
    }
    status = 'PIDDING'
    value = undefined
    reason = undefined
    resolve = value => {
        if (this.status !== 'PIDDING') return
        this.status = 'FULFILLED'
        this.value = value
    }
    reject = reason => {
        if (this.status !== 'PIDDING') return
        this.status = 'REFECTED'
        this.reason = reason
    }
    then(successCallBack, failCallBack) {
        if (this.status === 'FULFILLED') {
            successCallBack(this.value)
        } else if (this.status === 'REFECTED') {
            failCallBack(this.reason)
        } else {

        }
    }
}

module.exports = MyPromise