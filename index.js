// const promise = new Promise((resolve, reject) => {
//     resolve('成功')
//     // reject('失败')
// })
// promise.then(res => {
//     console.log('success', res)
// }, err => {
//     console.log('err', err)
// })

// promise.then(res => {
//     return new Promise((resolve, reject) => {
//         resolve('success')
//     })
// }).then(value => {
//     console.log(value)
// })

// // 不传

// promise.then(res => {
//     console.log('成功回调', res)
// }).catch(err => {
//     console.log('失败了', err)
// })

const MyPromise = require('./myPromise')

const promise = new MyPromise((resolve, reject) => {
    // resolve('promise 执行成功')
    reject('promise 执行失败')
})

promise.then(res => {
    console.log(res)
}, err => {
    console.log(err)
})